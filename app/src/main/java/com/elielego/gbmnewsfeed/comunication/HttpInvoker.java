package com.elielego.gbmnewsfeed.comunication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Eliel Martínez on 7/23/15.
 * A Generic HttpInvoker to make http petitions to a known url with (or not)
 * given params returning
 * a String JSON
 */
public class HttpInvoker extends AsyncTask<String, Void, String> {
    private String url;
    private List<NameValuePair> petitionParams;
    private String json = "";
    private InputStream is = null;

    public boolean isInternetError;
    public Context context;
    public PetitionService caller;
    private int result = Activity.RESULT_CANCELED;


    public HttpInvoker(Context context, PetitionService caller){
        this.context = context;
        this.caller = caller;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<NameValuePair> getPetitionParams() {
        return petitionParams;
    }

    public void setPetitionParams(List<NameValuePair> petitionParams) {
        this.petitionParams = petitionParams;
    }

    /**
     * Executes Http Petition with
     * @param url The url to excecute
     * @return String JSON
     */

    public String getJSONFromUrl(String url) {
        json = null;
        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);





            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 5000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 10000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            httpPost.setParams(httpParameters);


            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
        } catch(ConnectTimeoutException e){
            json = null;
            isInternetError = true;
            Log.e("Timeout Exception: ", e.toString());
        } catch(SocketTimeoutException ste){
            json = null;
            isInternetError = true;
            Log.e("Timeout Exception: ", ste.toString());
        } catch (UnsupportedEncodingException e) {
            json = null;
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            json = null;
            e.printStackTrace();
        } catch (IOException e) {
            json = null;
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection "+e.toString());
            json = null;
            e.printStackTrace();
        }finally{
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }

                json = sb.toString();
                is.close();
            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
                json = null;
            }
        }
        return json;

    }

    /**
     * Executes Http Petition with
     *
     * @param url The url to excecute
     * @param params given params
     * @return String JSON
     */

    public String getJSONFromUrl(String url, List<NameValuePair> params) {
        json = null;
        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 5000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 10000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            httpPost.setParams(httpParameters);
            httpPost.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
        } catch(ConnectTimeoutException e){
            json = null;
            isInternetError = true;
            Log.e("Timeout Exception: ", e.toString());
        } catch(SocketTimeoutException ste){
            json = null;
            isInternetError = true;
            Log.e("Timeout Exception: ", ste.toString());
        } catch (UnsupportedEncodingException e) {
            json = null;
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            json = null;
            e.printStackTrace();
        } catch (IOException e) {
            json = null;
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection "+e.toString());
            json = null;
            e.printStackTrace();
        }finally{
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }

                json = sb.toString();
                is.close();
            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
                json = null;
            }
        }
        return json;

    }

    @Override
    protected String doInBackground(String... params) {

        String JsonResponse = null;

        if (isInternetPresent()) {
            if (petitionParams == null) {
                JsonResponse = getJSONFromUrl(url);
            }else{
                JsonResponse = getJSONFromUrl(url,petitionParams);
            }
        }

        return JsonResponse;
    }



    @Override
    protected void onPostExecute(String response) {
        if (response != null){
            result = Activity.RESULT_OK;
            try {
                caller.publishResults(result,response);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            result = Activity.RESULT_CANCELED;
            try {
                caller.publishResults(result,response);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private boolean isInternetPresent() {

        String type = "";
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        //-- Use below code to determine connection type if needed
                        Boolean Is3g = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
                        Boolean IsWiFi = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
                        if (Is3g) {
                            type = "3G";
                        } else if (IsWiFi) {
                            type = "WIFI";
                        }
                        //---
                        return true;
                    }
            }

        }

        return false;
    }


}
