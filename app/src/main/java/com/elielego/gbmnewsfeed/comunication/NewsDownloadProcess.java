package com.elielego.gbmnewsfeed.comunication;

import com.elielego.gbmnewsfeed.models.News;

import java.util.List;

/**
 * Created by Eliel Martínez on 7/23/15.
 * Inteface that should be implemented for those classes that serve as
 * Delegate for the HttpInvoker
 *
 */
public interface NewsDownloadProcess {
    void processDownloadedNews(List<News> news);
}
