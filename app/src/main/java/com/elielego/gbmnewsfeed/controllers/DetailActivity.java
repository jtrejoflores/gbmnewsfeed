package com.elielego.gbmnewsfeed.controllers;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.elielego.gbmnewsfeed.R;

public class DetailActivity extends ActionBarActivity {
    String url;
    String title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_detail);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }



        Intent intent = getIntent();
        url = intent.getStringExtra("news_url");
        title = intent.getStringExtra("news_title");
        getSupportActionBar().setTitle(title);
        WebView detailWebView = (WebView) this.findViewById(R.id.webView);
        detailWebView.getSettings().setLoadsImagesAutomatically(true);
        detailWebView.getSettings().setJavaScriptEnabled(true);
        detailWebView.setWebViewClient(new DetailWebViewClient());
        detailWebView.loadUrl(url);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Noticia compartida por GBM News Feed  : " + url);
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Compartir con:"));
            return true;
        }else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class DetailWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //Return is false because is mandatory that new's detail opens in the webview
            return false;

        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        //closing transition animations
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }
}
