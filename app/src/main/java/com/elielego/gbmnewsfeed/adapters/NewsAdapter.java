package com.elielego.gbmnewsfeed.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.elielego.gbmnewsfeed.R;
import com.elielego.gbmnewsfeed.controllers.DetailActivity;
import com.elielego.gbmnewsfeed.models.News;
import com.elielego.gbmnewsfeed.utils.ImageLoader;

import java.util.List;

/**
 * Created by Eliel Martínez on 7/23/15.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> implements CardView.OnClickListener{
    private List<News> news;
    private Context context;

    public static final int DIMENTION_THUMB_IMG = 1900;
    public static final int DEFAULT_IMG_QUALITY = 75;

    private static int desiredWidth = DIMENTION_THUMB_IMG;
    private static int desiredQuality = DEFAULT_IMG_QUALITY;

    public NewsAdapter(List<News> news, Context context){
        this.news = news;
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        News newsObject = (News)v.getTag();
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra("news_url",newsObject.getUrl());
        intent.putExtra("news_title",newsObject.getTitle());
        context.startActivity(intent);
    }

    public static class NewsViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView newsTitle;
        TextView kwic;
        TextView author;
        ImageView newsPhoto;
        News news;

        NewsViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card_view);
            newsTitle = (TextView)itemView.findViewById(R.id.txt_title);
            kwic = (TextView)itemView.findViewById(R.id.txt_kwic);
            author = (TextView)itemView.findViewById(R.id.txt_author);
            newsPhoto = (ImageView)itemView.findViewById(R.id.news_photo);

        }
    }

    @Override
    public NewsAdapter.NewsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // create a new view
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_card_view, viewGroup, false);
        NewsViewHolder pvh = new NewsViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(NewsAdapter.NewsViewHolder viewHolder, int i) {
        viewHolder.cv.setTag(news.get(i));
        viewHolder.newsTitle.setText(news.get(i).getTitle());
        viewHolder.kwic.setText(news.get(i).getKwic());
        viewHolder.author.setText(news.get(i).getAuthor());
        ImageLoader imgLoader = new ImageLoader(context);
        //holder.imgSolucion.setImageResource(holder.drawableId);
        imgLoader.DisplayImage(news.get(i).getIurl(), viewHolder.newsPhoto, desiredWidth, desiredQuality );
        viewHolder.cv.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
